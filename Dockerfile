FROM tomcat:8.5.4-jre8

MAINTAINER Jonathan Dupuich

RUN apt-get update && \
	apt-get install -y vim

RUN rm -rf webapps/**
COPY mysql-connector-java-5.1.39-bin.jar lib/
